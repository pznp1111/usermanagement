
const userRoutes = (app, fs) => {

    // variables
    const dataPath = './data/users.json';

    // helper methods
    const readFile = (callback, returnJson = false, filePath = dataPath, encoding = 'utf8') => {
        fs.readFile(filePath, encoding, (err, data) => {
            if (err) {
                throw err;
            }

            callback(returnJson ? JSON.parse(data) : data);
        });
    };

    const writeFile = (fileData, callback, filePath = dataPath, encoding = 'utf8') => {

        fs.writeFile(filePath, fileData, encoding, (err) => {
            if (err) {
                throw err;
            }

            callback();
        });
    };

    const cors = require('cors');
    app.use(cors());

    // READ
    app.get('/users', (req, res) => {
        fs.readFile(dataPath, 'utf8', (err, data) => {
          var dateFormat = require('dateformat');
          const keys = Object.values(JSON.parse(data))
          var result = []
          for (const key of keys) {
            var tempDate = new Date(key.dob);
            key['dobstr'] = dateFormat(tempDate,"yyyy-mm-dd");
            result.push(key);
             if (key.id == req.query["id"]){
                 res.send(key);
                 canFind = true;
             }
          }
          res.send(result);

        });
    });

    // CREATE
    app.post('/users', (req, res) => {
        fs.readFile(dataPath, 'utf8', (err, data) => {
          const keys = Object.values(JSON.parse(data))
          var canFind = false;
          for (const key of keys) {
            console.log("key",key)
             if (key.id == req.body["id"]){
                 res.send(key);
                 canFind = true;
             }
          }

        });
    });


    app.post('/users', (req, res) => {
        fs.readFile(dataPath, 'utf8', (err, data) => {
          const keys = Object.values(JSON.parse(data))
          var canFind = false;
          for (const key of keys) {
             if (key.id == req.body["id"]){
                 res.send(key);
                 canFind = true;
             }
          }

        });
    });


    // CREATE
      app.post('/addusers', (req, res) => {

          readFile(data => {
              const newUserId = Object.keys(data).length + 1;
              data[newUserId.toString()] = {
                "id": newUserId,
                "firstName": req.body.firstName,
                "lastName": req.body.lastName,
                "email": req.body.email,
                "dob": req.body.dob
              };
              writeFile(JSON.stringify(data, null, 2), () => {
                  res.status(200).send('new user added');
              });
          },
              true);
      });



    // UPDATE
    app.put('/users', (req, res) => {

        var moment = require('moment')
        readFile(data => {
            for (let key in data) {
              if(data[key].id == req.body.id){
                data[key] = {
                  "id": req.body.id,
                  "firstName": req.body.firstName,
                  "lastName": req.body.lastName,
                  "email": req.body.email,
                  "dob": moment(req.body.dobstr, "YYYY-MM-DD").toDate().getTime()
                }
              }
            }
            writeFile(JSON.stringify(data), () => {
              res.status(200).send(`data updated`);
            })

        },
            true);
    });


    // DELETE
    app.delete('/users/:id', (req, res) => {
        readFile(data => {
            for (let key in data) {
              if(data[key].id == req.params.id){
                delete data[key];
              }
            }
            writeFile(JSON.stringify(data), () => {
                res.status(200).send(`users removed`);
            });
        },
            true);
    });
};

module.exports = userRoutes;
