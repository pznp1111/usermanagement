import React, { Component,Fragment } from 'react';
//import Login from './Login';
import { Redirect } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import AppBar from 'material-ui/AppBar';
import './App.css';
import Grid from '@material-ui/core/Grid';


class Loginscreen extends Component {
  constructor(props){
    super(props);
    this.state={
      username:'',
      password:'',
      loginscreen:[],
      loginmessage:'',
      buttonLabel:'Register',
      isLogin:true,
      redirectToLogin: false
    }
  }


  componentWillMount(){
      var loginscreen=[];
      this.setState({loginscreen:loginscreen})
        }

  validateEmail(email){
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
  }


  handleClickSubmit(){
    //event.preventDefault();
    // const [blahData] = useFetch('/user/blahblahblah');
    // const [unauthData] = useFetch('/user/unauthorised-user');


    var loginData = {
      "email":this.state.username,
      "password":this.state.password
    }

    if(!this.validateEmail(this.state.username)){
      this.setState({ loginmessage: "invalid email" });
      return;
    }

    if(this.state.username === 'dev@dev.com'  && this.state.password === 'dev123'){
      this.setState({ redirectToLogin: true });
    } else{
      this.setState({ loginmessage: "invalid username or password" });
    }
    console.log("loginData",loginData);
  }


  render() {
    const { username,redirectToLogin } = this.state;

      return (
        <div className="App">

        <Fragment>
        {redirectToLogin &&
          <Redirect to={{
            pathname: "/dashboard",
            state: {
              username,
              message: "logging now",
            }
          }} push />
        }
        <div className="loginscreen">
          <div>

            <MuiThemeProvider>

              <div>
              <AppBar title="Login" style={{ background: '#2E3B55' }}/>

               <TextField
                 hintText="Enter your EMail"
                 floatingLabelText="EMail"
                 onChange = {(event,newValue) => this.setState({username:newValue})}
                 />
               <br/>
                 <TextField
                   type="password"
                   hintText="Enter your Password"
                   floatingLabelText="Password"
                   onChange = {(event,newValue) => this.setState({password:newValue})}
                   />
                 <br/>
                 <RaisedButton label="Submit" primary={true} style={style} onClick={(event) => this.handleClickSubmit()}/>

                 <div className="root">
                  <Grid container spacing={3}>
                    <Grid item xs={12} id = 'errorNotification'>
                      {this.state.loginmessage}
                    </Grid>
                </Grid>
                </div>

             </div>
             </MuiThemeProvider>
          </div>

        </div>

        </Fragment>
        </div>

      );
          }
}

const style = {
  margin: 15,
};

export default Loginscreen;
