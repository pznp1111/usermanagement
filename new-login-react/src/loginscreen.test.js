import React from 'react';
import { shallow, mount } from 'enzyme';
import Loginscreen from './Loginscreen';
import { BrowserRouter } from 'react-router-dom/cjs/react-router-dom';

// Shallowly tests component using jest and enzyme, ignoring any child components
it('shallowly renders without crashing', () => {
  shallow(<Loginscreen />);
});


it('correctly sets big error message', () => {
  const wrapper = shallow(<Loginscreen />);

    wrapper.setState({ username: 'bar' });
    wrapper.setState({ password: 'bar' });
  //expect(wrapper.find('#errorNotification')).to.have.lengthOf(0);
  // expect(wrapper.find(Notification).length).toBe(1);
  wrapper.instance().handleClickSubmit();
  console.log("wrapper.instance().state",wrapper.instance().state);
  expect(wrapper.instance().state.loginmessage).toBe("invalid username or password");
})

it('correctly login with default account', () => {
  const wrapper = shallow(<Loginscreen />);


  wrapper.setState({ username: 'dev@dev.com' });
  wrapper.setState({ password: 'dev123' });

  //expect(wrapper.find('#errorNotification')).to.have.lengthOf(0);
  // expect(wrapper.find(Notification).length).toBe(1);
  wrapper.instance().handleClickSubmit();
  console.log("wrapper.instance()",wrapper.instance().state);
  expect(wrapper.instance().state.redirectToLogin).toBe(true);
  //expect(mockEvent.preventDefault).toHaveBeenCalled();
})



//
// //do some test
// it('renders without crashing', () => {
//   mount(
//     <BrowserRouter>
//       <Loginscreen />
//     </BrowserRouter>
//   );
// });
