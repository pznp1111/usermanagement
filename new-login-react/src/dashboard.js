import React, { Component } from 'react';
import MaterialTable from 'material-table';
import axios from 'axios';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import Grid from '@material-ui/core/Grid';
import './App.css';
const _ = require('lodash');

class Dashboard extends Component {
  constructor(props){
    super(props);
    this.state={
    username:'',
    password:'',
    message:'',
    columns: [
      { title: 'First Name', field: 'firstName'},
      { title: 'Last Name', field: 'lastName' },
      { title: 'email', field: 'email' },
      { title: 'Date Of Birth', field: 'dobstr',
      editComponent: ({ value, onChange }) => (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            margin="normal"
            id="time-picker"
            label="Date picker"
            format="yyyy-MM-dd"
            value={value}
            onChange={onChange}
            KeyboardButtonProps={{
              "aria-label": "change time"
            }}
          />
        </MuiPickersUtilsProvider>
      )
     },
    ],
    data: [{
        "id": 1,
        "firstName": "John111111",
        "lastName": "Doe1",
        "email": "john1.doe@gmail.com",
        "dob": 1960200000,
        "dobstr": "1970-01-24"
    },
    {
        "id": 2,
        "firstName": "John2",
        "lastName": "Doe2",
        "email": "john2.doe@gmail.com",
        "dob": 2595493789,
        "dobstr": "1970-01-31"
    },
    {
        "id": 3,
        "firstName": "222",
        "lastName": "333",
        "email": "444@ggg.com",
        "dob": 1599376033729,
        "dobstr": "2020-09-06"
    },
    {
        "id": 4,
        "firstName": "eee",
        "lastName": "444",
        "email": "222@kk.ccc",
        "dob": 1504682376957,
        "dobstr": "2017-09-06"
    }]
    }
   }

  componentDidMount() {
    return     axios.get('http://localhost:3001/users').then(res => {
          console.log(res);
          this.setState({data:res.data});
          console.log("data is",this.state.data);
        });
  }

  loadData = () =>{
    axios.get('http://localhost:3001/users').then(res => {
      console.log(res);
      this.setState({data:res.data});
      console.log("data is",this.state.data);
      return res;
    });
  }

  onChangeDate = (props,key) =>{
    console.log("onChangeDate",key);
    console.log("props",props);
  }

  validateField = (rowData) => {
    this.setState({message:""});
    var isTrue = true;
    console.log("validateField",rowData);
    if(rowData.firstName.length > 50 || rowData.firstName.length  <= 0){
      isTrue = false;
      this.setState({message:"FirstName field cannot be more than 50 char"});
      return;
    }
    if(rowData.lastName.length > 50 || rowData.lastName.length <= 0){
      isTrue = false;
      this.setState({message:"LastName field cannot be more than 50 char"});
      return;
    }

    isTrue = this.validateEmail(rowData.email);
    if(isTrue){
      var temp = this.state.data;
      //console.log("test temp",temp);
      var index = _.findIndex(temp,{id:rowData.id});
      temp.splice(index,1,rowData);
      this.setState({data:temp});
      axios.put('http://localhost:3001/users', rowData).then(res => {
          console.log(res);
          if(res.status === 200){
            this.loadData();
          }
        });
    }else{
      this.setState({message:"invalid email format"});
      return;
    }
  }

  validateEmail = (email) => {
      const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
  }

  validateFieldAdd = (rowData) => {
    this.setState({message:""});
    var isTrue = true;
    var moment = require('moment');

    if(rowData.dob === undefined){
      const moonLanding = new Date();
      rowData.dob = moonLanding.getTime();
    }
    if(rowData.dobstr !== undefined){
      rowData.dob = moment(rowData.dobstr, "YYYY-MM-DD").toDate().getTime();
    }

    if(rowData.firstName === undefined || rowData.lastName === undefined || rowData.email === undefined){
      isTrue = false;
      this.setState({message:"please fill in all fields!"});
      return;
    }
    if(rowData.firstName.length > 50 || rowData.firstName.length <= 0){
      isTrue = false;
      this.setState({message:"FirstName field cannot be more than 50 char"});
      return;
    }
    if(rowData.lastName.length > 50 || rowData.lastName.length <= 0){
      isTrue = false;
      this.setState({message:"LastName field cannot be more than 50 char"});
      return;
    }

    // const expression = /(?!.*\.{2})^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([\t]*\r\n)?[\t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([\t]*\r\n)?[\t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;

    //isTrue = expression.test(rowData.email.toLowerCase());
    isTrue = this.validateEmail(rowData.email);
    if(isTrue){
      console.log("send to backend");
      var temp = this.state.data;
      temp.push(rowData);
      this.setState({data:temp});
      axios.post('http://localhost:3001/addusers', rowData).then(res => {
          console.log(res);
          if(res.status === 200){
            this.loadData();
          }
        });
    } else{
      this.setState({message:"invalid email format"});
      return;
    }
  }

  deleteField = (rowData) =>{
    this.setState({message:""});
    var temp = this.state.data;
    var index = _.findIndex(temp,{id:rowData.id});
    temp.splice(index,1);
    this.setState({data:temp});


    axios.delete('http://localhost:3001/users/'+rowData.id).then(res => {
        if(res.status === 200){
          this.loadData();
        }
      });
  }

  render() {
    return (
        <div>
          <MaterialTable title="User Management" columns={this.state.columns} data={this.state.data}
            editable={{
              onRowAdd: (newData) =>
                new Promise((resolve) => {
                  setTimeout(() => {
                    resolve();
                    this.validateFieldAdd(newData);
                  }, 600);
                }),
              onRowUpdate: (newData, oldData) =>
                new Promise((resolve) => {
                  setTimeout(() => {
                    resolve();
                    //var dateFormat = require('dateformat');
                  //  var localDate = new Date(newData.dobstr);
                    this.validateField(newData);
                  }, 600);
                }),

                onRowDelete: (oldData) =>
                  new Promise((resolve) => {
                    setTimeout(() => {
                      resolve();
                      this.deleteField(oldData);
                    }, 600);
                  }),
                }}
              />

        <div className="root">
        <Grid container spacing={3}>
           <Grid item xs={12}>
             {this.state.message}
           </Grid>
        </Grid>
        </div>


      </div>
    );
  }
}
export default Dashboard;
