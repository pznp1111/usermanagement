import axios from 'axios';
// const fs = require('fs');
// const promisify = require('util').promisify;
const serverError = 'Server API Error!'










    export const get = ({
            url,
            msg = serverError,
            config = {
                'Content-Type': 'application/json; charset=utf-8',
                // 'Access-Control-Allow-Headers' : 'Content-Type, api_key, Authorization',
                // 'Access-Control-Allow-Methods' : '*',
                // 'Access-Control-Allow-Origin' : '*'
            }
        }) =>
        axios
        .get(url, config)
        .then(res => {
            console.log(res);
            if (res.data === undefined || res.data === null) {
                console.log(msg);
              //  message.error(msg);
                return [];
            } else if (res.status !== 200) {
                console.log(res.statusText);
              //  message.error(res.message);
                return [];
            } else {
                return res.data;
            }
        })
        .catch(err => {
            console.log(err);
            //message.warn(msg);
            return [];
        });

    export const post = ({
            url,
            data,
            msg = serverError,
            config
        }) =>
        axios
        .post(url, data, config)
        .then(res => {
            console.log(res);
            if (res.data === undefined || res.data === null) {
                console.log(msg);
              //  message.error(msg);
                return [];
            } else if (res.status !== 200) {
                console.log(res.statusText);
              //  message.error(res.message);
                return [];
            } else if (res.data.statusCode !== 200) {
                console.log(res.data.message);
              //  message.error(res.data.message);
                return [];
            } else {
                return res.data;
            }
        })
        .catch(err => {
            console.log(err);
            //message.error(msg);
            return [];
        });

        var doSome = new Promise(function(resolve, reject){
          console.log("dir",__dirname);
            resolve('I am doing something');
          
        });
        export const mock_post = ({
                url,
                data,
                msg = serverError,
                config
            }) =>
            doSome
            .then(res => {
                console.log(res);
                return [123];
                //https://stackoverflow.com/questions/53449406/write-to-a-text-or-json-file-in-react-with-node
                // if (res.data === undefined || res.data === null) {
                //     console.log(msg);
                //   //  message.error(msg);
                //     return [];
                // } else if (res.status !== 200) {
                //     console.log(res.statusText);
                //   //  message.error(res.message);
                //     return [];
                // } else if (res.data.statusCode !== 200) {
                //     console.log(res.data.message);
                //   //  message.error(res.data.message);
                //     return [];
                // } else {
                //     return res.data;
                // }
            })
            .catch(err => {
                console.log(err);
                //message.error(msg);
                return [];
            });
