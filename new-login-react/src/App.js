import React, { Component }  from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import LoginScreen from './Loginscreen';
import Dashboard from './dashboard';



class App extends Component {
  constructor(props){
    super(props);
    this.state={
      loginPage:[]
    }
  }

  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" children={({ match, location }) => <LoginScreen match={match} location={location} />} />
          <Route exact path="/dashboard" children={({ match, location }) => <Dashboard match={match} location={location} />} />
        </Switch>
      </Router>
    );
  }
}

export default App;
