import React from "react";
import axios from 'axios';
import { shallow, mount } from 'enzyme';
// import { fetchData, API,postDataAdd,deleteData,APIAddUser } from './dashboard';
import Dashboard from './dashboard';
import { createMount } from "@material-ui/core/test-utils";

import MaterialTable from 'material-table';

const fieldProps = {
  // placeholder: "A placeholder",
  onRowAdd: jest.fn(),
  onRowUpdate: jest.fn(),
  onRowDelete: jest.fn(),
};


const setup = () => {
  const wrapper = mount(
    <Dashboard {...fieldProps} />,
    {
    //  context: {muiTheme},
      childContextTypes: {MaterialTable: React.PropTypes.object}
    }
  );

  return {
  //  props,
    wrapper,
  };
};


describe("SearchField", () => {
  // let mount;

  // beforeEach(() => {
  //   mount = createMount();
  // });
  //
  // afterEach(() => {
  //   mount.cleanUp();
  // });





  it("renders a  component ", () => {
    const wrapper = shallow(<Dashboard />);
    expect(wrapper.find(MaterialTable).length).toBe(1);
  });

  it("add user ", () => {
    const wrapper = shallow(<Dashboard />);
    wrapper.instance().validateFieldAdd({
        "id": 5,
        "firstName": "eee",
        "lastName": "444",
        "email": "222@kk.ccc",
        "dob": 1504682376957,
        "dobstr": "2017-09-06"
    });
    console.log("wrapper.instance()",wrapper.instance());
    expect(wrapper.instance().state.data.length).toBe(5);
  });

  it("modify field", () => {
    const wrapper = shallow(<Dashboard />);
    // console.log("wrapper.instance()",wrapper.instance().state.data);
    wrapper.instance().validateField({
        "id": 4,
        "firstName": "eee1",
        "lastName": "444",
        "email": "222@kk.ccc",
        "dob": 1504682376957,
        "dobstr": "2017-09-06"
    });
    //console.log("wrapper.instance()",wrapper.instance().state.data);
    expect(wrapper.instance().state.data[3].firstName).toBe("eee1");
  });


  it("delete field", () => {
    const wrapper = shallow(<Dashboard />);
    // console.log("wrapper.instance()",wrapper.instance().state.data);
    wrapper.instance().deleteField({
        "id": 1,
        "firstName": "eee1",
        "lastName": "444",
        "email": "222@kk.ccc",
        "dob": 1504682376957,
        "dobstr": "2017-09-06"
    });
    console.log("wrapper.instance()",wrapper.instance().state.data);
    expect(wrapper.instance().state.data.length).toBe(3);
  });


//
//   it("renders a <TextField/> component with expected props", () => {
//     const wrapper = mount(<Dashboard  {...fieldProps}/>);
//     // expect(wrapper.props().placeholder).toEqual("A placeholder");
//     //console.log("wrapper props",wrapper.props());
//     expect(wrapper.props().onRowAdd).toBeDefined();
//     expect(wrapper.props().onRowUpdate).toBeDefined();
//     expect(wrapper.props().onRowDelete).toBeDefined();
//   });
//   it("should trigger onChange on <SearchField/> on key press", () => {
//     const wrapper = mount(<Dashboard {...fieldProps}/>);
//     console.log("wrapper input",wrapper.props().onRowAdd);
//
//     wrapper.props().onRowAdd({
//         "id": 5,
//         "firstName": "eee",
//         "lastName": "444",
//         "email": "222@kk.ccc",
//         "dob": 1504682376957,
//         "dobstr": "2017-09-06"
//     })
//     console.log("wrapper input 2",wrapper.props());
//     console.log("wrapper input 3",wrapper.instance());
//     // wrapper.find("input").simulate("change");
//     // expect(fieldProps.onRowAdd).toHaveBeenCalled();
//   });
});

// jest.mock('axios');
//
// describe('fetchData', () => {
//   it('fetches successfully data from an API', async () => {
//     const data = [
//     {
//         "id": 1,
//         "firstName": "John111111",
//         "lastName": "Doe1",
//         "email": "john1.doe@gmail.com",
//         "dob": 1960200000,
//         "dobstr": "1970-01-24"
//     },
//     {
//         "id": 2,
//         "firstName": "John2",
//         "lastName": "Doe2",
//         "email": "john2.doe@gmail.com",
//         "dob": 2595493789,
//         "dobstr": "1970-01-31"
//     },
//     {
//         "id": 3,
//         "firstName": "222",
//         "lastName": "333",
//         "email": "444@ggg.com",
//         "dob": 1599376033729,
//         "dobstr": "2020-09-06"
//     },
//     {
//         "id": 4,
//         "firstName": "eee",
//         "lastName": "444",
//         "email": "222@kk.ccc",
//         "dob": 1504682376957,
//         "dobstr": "2017-09-06"
//     }
// ]
//
//
//     axios.get.mockImplementationOnce(() => Promise.resolve(data));
//     var test = await fetchData('');
//     await expect(test.length).toEqual(4);
//
//     expect(axios.get).toHaveBeenCalledWith(
//       `${API}`,
//     );
//   });
// });
//
//
// describe('post  Data', () => {
//   it('add data from an API', async () => {
//     var data = {
//         "firstName": "ee2e",
//         "lastName": "444",
//         "email": "222@kk.ccc",
//         "dob": 1504682376957,
//         "dobstr": "2017-09-07"
//     }
//     axios.post.mockImplementationOnce(() => Promise.resolve(data));
//     var test = await postDataAdd(data);
//     expect(postDataAdd.length).toBe(1);
//   });
// });
//
//
//
// describe('delete Data', () => {
//
//
//   it('delete from an API', async () => {
//
//     axios.delete.mockImplementationOnce(() => Promise.resolve("1"));
//     var test = await deleteData("1");
//     expect(postDataAdd.length).toBe(1);
//   });
// });
