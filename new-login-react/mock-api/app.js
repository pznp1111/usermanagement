// const express = require('express');
// //const apiMocker = require('connect-api-mocker');
// const bodyParser = require('body-parser')
//
// const port = 9000;
//
// const low = require('lowdb')
// const FileSync = require('./FileSync')
// const adapter new FileSync('db.json')
// const db = low(adapter)
// const app = express();
// // Uncomment if not using create-react-app
// const cors = require('cors');
// app.use(cors());
//
// // app.use('/api', apiMocker('mock-api'));
// //
// // console.log(`Mock API Server is up and running at: http://localhost:${port}`);
// // app.listen(port);
//
// app.use(bodyParser.json())
// low(adapter)
//   .then(db => {
//     // Routes
//     // GET /posts/:id
//     app.get('/posts/:id', (req, res) => {
//       const post = db.get('posts')
//         .find({ id: req.params.id })
//         .value()
//
//       res.send(post)
//     })
//
//     // POST /posts
//     app.post('/posts', (req, res) => {
//       db.get('posts')
//         .push(req.body)
//         .last()
//         .assign({ id: Date.now().toString() })
//         .write()
//         .then(post => res.send(post))
//     })
//
//     // Set db default values
//     return db.defaults({ posts: [] }).write()
//   })
//   .then(() => {
//     app.listen(3000, () => console.log('listening on port 3000'))
//   })



const express = require('express')
const bodyParser = require('body-parser')
const low = require('lowdb')
const FileAsync = require('./FileAsync')

// Create server
const app = express()
app.use(bodyParser.json())

// Create database instance and start server
const adapter = new FileAsync('db.json')
low(adapter)
  .then(db => {
    // Routes
    // GET /posts/:id
    app.get('/posts/:id', (req, res) => {
      console.log("adapter",adapter);
      const post = db.get('posts')
        .find({ id: req.params.id })
        .value()

      res.send(post)
    })

    // POST /posts
    app.post('/posts', (req, res) => {
            console.log("adapter",adapter);
      db.get('posts')
        .push(req.body)
        .last()
        .assign({ id: Date.now().toString() })
        .write()
        .then(post => res.send(post))
    })

    // Set db default values
    return db.defaults({ posts: [] }).write()
  })
  .then(() => {
    app.listen(3000, () => console.log('listening on port 3000'))
  })
